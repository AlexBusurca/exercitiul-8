package Ro.Orange;

/*• Create a single class with a main method
  • Create a list called MyHotelEmployees and create a new ArrayList (you will have to import
    the interfaces)
  • Create an instance of an arraylist and asing it to a variable of type list
  • Add some elements to that list , like employees using the add() method
  • Use the add method several times just to make sure you have enough employees for later use
  • Get the size of the employees by using the size()method and print it
  • Print the employees name
  • Print the employee at the 1st floor by using his index number and after ,remove him by
    calling the method remove() assigning to his index(eg. myList.remove(0))
  • Print the new list of employees
  • Scroll through all employees using the for loop
  • Add a new employee replacing the old employee at the 1st floor using set() method for each
    employee sort by floor
  • The final step is to print the sorted employees*/

//import the interfaces
import java.util.ArrayList;
import java.util.List;

//Single class with main method
public class Main {

    public static void main(String[] args) {

        // create a list called MyHotelEmployees and create a new ArrayList
        //Create an instance of an arraylist and asing it to a variable of type list
        List MyHotelEmployees = new ArrayList();

        // adding 7 elements to the list
        MyHotelEmployees.add("Will Smith");
        MyHotelEmployees.add("Bruce Willis");
        MyHotelEmployees.add("Chuck Norris");
        MyHotelEmployees.add("Jean Claude Van Damme");
        MyHotelEmployees.add("Dwayne Johnson");
        MyHotelEmployees.add("Sylvester Stallone");
        MyHotelEmployees.add("Jason Statham");

        //get the size of the list and print it to the console
        System.out.println("This hotel has " + MyHotelEmployees.size() + " employees.");

        //print the name of the employees to the console
        System.out.println("Their names are: " + MyHotelEmployees.toString());

        //Print the employee at the 1st floor by using his index number
        System.out.println("The employee at the first floor is " + MyHotelEmployees.get(1));

        //remove the employee at the first floor by calling the method remove() assigning to his index
        MyHotelEmployees.remove(1);
        System.out.println("The designated employee on the first floor was fired for inappropriate behaviour.");

        //Print the new list of employees
        System.out.println("The new list of employees is: " + MyHotelEmployees.toString());

        //print the new number of hotel employees
        System.out.println("The new number of employees of the hotel is: " + MyHotelEmployees.size());

        //Scroll through all employees using the for loop
        int noOfEmployees = MyHotelEmployees.size();
        for (int i = 0; i < noOfEmployees; i++) {
            System.out.println(MyHotelEmployees.get(i));
        }

        //Add a new employee replacing the old employee at the 1st floor using set() method for each
        //employee sort by floor
            MyHotelEmployees.set(1,"Arnold Schwarzenneger");
            System.out.println("The new substitute will be " + MyHotelEmployees.get(1));

        //The final step is to print the sorted employees
        System.out.println("The final list of employees is: " + MyHotelEmployees.toString());

    }
}

