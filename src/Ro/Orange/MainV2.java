package Ro.Orange;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class MainV2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<String> Employees = new ArrayList<>();
        Iterator it = Employees.iterator();
        System.out.println("To enter a new employee, type new then press enter and then type his name. \n" +
                           "When you are happy with the number of employees entered type exit to finish.");
        while(input.hasNext()) {
            if (input.nextLine().contains("exit")) {
                break;
            }
            String name = input.nextLine();
            Employees.add(name);
            System.out.println("Your input was " + name.toString() + ". Please enter the next name or type exit.");
        }


        System.out.println("Thank you for your input. You have entered " + Employees.size() + " employees for this hotel \n" +
                " and their names are:" + Employees.toString());

    }
}
